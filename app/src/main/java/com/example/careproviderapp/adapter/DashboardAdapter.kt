package com.example.careproviderapp.adapter

import android.content.Context
import android.graphics.*
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.careproviderapp.R
import com.example.careproviderapp.helpers.DashboardCategory
import kotlinx.android.synthetic.main.activity_dashboard_item.view.*
import java.util.ArrayList

class DashboardAdapter(private val catArrayList: ArrayList<DashboardCategory>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var itemClickListener: OnItemClickListener
    private var context: Context? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, obj: DashboardCategory, position: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.itemClickListener = mItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.activity_dashboard_item, parent, false)

        return PlaceViewHolder(itemView)
    }

    override fun onBindViewHolder(reholder: RecyclerView.ViewHolder, position: Int) {

        if (reholder is PlaceViewHolder) {
            val cats = catArrayList[position]

            reholder.catNameTextView.text = cats.name

            context = reholder.catImageView.context

            val idCatImage = getDrawableInt(context!!, cats.image)
            setImageToImageView(context!!, reholder.catImageView, idCatImage)

            setCircleImageToImageView(context!!, reholder.catbackgroundImageView, R.drawable.white_background, 0, 0)

            val color = Color.parseColor(cats.color.trim())
            reholder.catbackgroundImageView.setColorFilter(color, PorterDuff.Mode.SRC_IN)

            reholder.catbackgroundImageView.setOnClickListener { view ->
                itemClickListener.onItemClick(view, catArrayList[position], position)

            }

        }

    }

    override fun getItemCount(): Int {
        return catArrayList.size
    }

    inner class PlaceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var catImageView: ImageView
        var catNameTextView: TextView
        var catbackgroundImageView: ImageView

        init {
            catImageView = view.catImageView
            catNameTextView = view.catNameTextView
            catbackgroundImageView = view.catbackgroundImageView
        }
    }


    fun getDrawableInt(context: Context?, name: String?): Int {
        return context?.resources!!.getIdentifier(name, "drawable", context.packageName)
    }

    fun setImageToImageView(context: Context, imageView: ImageView, drawable: Int) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
            .skipMemoryCache(true)

        Glide.with(context)
            .load(drawable)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageView)
    }

    fun setCircleImageToImageView(context: Context?, imageView: ImageView, drawable: Int, borderWidth: Int, color: Int) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
            .skipMemoryCache(true)
            .circleCrop()

        if (borderWidth > 0) {
            Glide.with(context!!)
                .load(drawable)
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(object : SimpleTarget<Drawable>() {
                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {

                        imageView.setImageDrawable(resource)

                        try {
                            val colorContextCompat = ContextCompat.getColor(context, color)


                            val bitmap = (resource as BitmapDrawable).bitmap

                            if (bitmap != null) {

                                val d = BitmapDrawable(context.resources, getCircularBitmapWithBorder(bitmap, borderWidth, colorContextCompat))

                                imageView.setImageDrawable(d)
                            } else {
                                imageView.setImageDrawable(resource)
                            }
                        } catch (e: Exception) {
                            Log.e("TEAMPS", "onResourceReady: ", e)
                        }

                    }
                })
        } else {
            Glide.with(context!!)
                .load(drawable)
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
        }
    }

    fun getCircularBitmapWithBorder(bitmap: Bitmap?,
                                    borderWidth: Int, color: Int): Bitmap? {
        if (bitmap == null || bitmap.isRecycled) {
            return null
        }

        val width = bitmap.width + borderWidth
        val height = bitmap.height + borderWidth

        val canvasBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        val paint = Paint()
        paint.isAntiAlias = true
        paint.shader = shader

        val canvas = Canvas(canvasBitmap)
        val radius = if (width > height) height.toFloat() / 2f else width.toFloat() / 2f
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius, paint)
        paint.shader = null
        paint.style = Paint.Style.STROKE
        paint.color = color
        paint.strokeWidth = borderWidth.toFloat()
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius - borderWidth / 2, paint)
        return canvasBitmap
    }

}
package com.example.careproviderapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.careproviderapp.CreateAccount
import com.example.careproviderapp.R
import com.example.careproviderapp.helpers.Place
import kotlinx.android.synthetic.main.activity_pending_appt_item.view.*
import java.lang.Exception
import java.util.*


class PendingApptAdapter(private val placeArrayList: ArrayList<Place>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var itemClickListener: OnItemClickListener

    interface OnItemClickListener {
        fun onItemClick(view: View, obj: Place, position: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.itemClickListener = mItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.activity_pending_appt_item, parent, false)

        return PlaceViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (viewHolder is PlaceViewHolder) {

            val place = placeArrayList[position]

            viewHolder.pendingUserName.text = place.name

            val context = viewHolder.placeHolderCardView.context

            val id = getDrawableInt(context, place.imageName)
            setImageToImageView(context, viewHolder.pendingListItemIcon, id)

            viewHolder.pendingAddress1.text = place.type
            viewHolder.pendingAddress2.text = place.address
            viewHolder.pendingMobile.text = place.phone
            viewHolder.pendingDetails.text = place.desc

            /*viewHolder.placeRatingBar.rating = java.lang.Float.parseFloat(place.totalRating)
            viewHolder.totalRatingTextView.text = place.totalRating
            viewHolder.ratingCountTextView.text = place.ratingCount*/

            /*if (Integer.parseInt(place.discount) > 0) {
                viewHolder.promoCardView.visibility = View.VISIBLE
                val discount = place.discount + " %"
                viewHolder.promoAmtTextView.text = discount
            } else {
                viewHolder.promoCardView.visibility = View.GONE
            }*/


            viewHolder.placeHolderCardView.setOnClickListener { v: View -> itemClickListener.onItemClick(v, placeArrayList[position], position) }

            viewHolder.pendingListAccept.setOnClickListener{
                try {

                }
                catch (ignored: Exception) {

                }
            }

            viewHolder.pendingListReject.setOnClickListener {
                try {

                }
                catch (ignored: Exception) {

                }
            }

        }
    }

    override fun getItemCount(): Int {
        return placeArrayList.size
    }

    inner class PlaceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var pendingListItemIcon: ImageView = view.pendingListItemIcon
        var pendingUserName: TextView = view.pendingUserName
        var pendingAddress1: TextView = view.pendingAddress1
        var pendingAddress2: TextView = view.pendingAddress2
        var pendingMobile: TextView = view.pendingMobile
        var pendingDetails: TextView = view.pendingDetail
        var pendingListReject: ImageView = view.pendingListReject
        var pendingListAccept: ImageView = view.pendingListAccept
        var placeHolderCardView: CardView = view.placeHolderCardView

        /*var totalRatingTextView: TextView = view.totalRatingTextView
        var ratingCountTextView: TextView = view.ratingCountTextView
        var placeRatingBar: RatingBar = view.placeRatingBar
        var promoAmtTextView: TextView = view.promoAmtTextView
        var promoCardView: CardView = view.promoCardView
        var placeHolderCardView: CardView = view.placeHolderCardView*/

    }

    fun setImageToImageView(context: Context, imageView: ImageView, drawable: Int) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
            .skipMemoryCache(true)

        /*Glide.with(context)
            .load(drawable)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageView)*/
    }

    fun getDrawableInt(context: Context?, name: String?): Int {
        return context?.resources!!.getIdentifier(name, "drawable", context.packageName)
    }
}

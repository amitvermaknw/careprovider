package com.example.careproviderapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.careproviderapp.R
import com.example.careproviderapp.helpers.Place
import kotlinx.android.synthetic.main.activity_upcoming_appt_item.view.*
import java.lang.Exception
import java.util.*


class UpcomingApptAdapter(private val placeArrayList: ArrayList<Place>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var itemClickListener: OnItemClickListener

    interface OnItemClickListener {
        fun onItemClick(view: View, obj: Place, position: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.itemClickListener = mItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.activity_upcoming_appt_item, parent, false)

        return PlaceViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (viewHolder is PlaceViewHolder) {

            val place = placeArrayList[position]

            viewHolder.upcomingUserName.text = place.name

            val context = viewHolder.placeHolderCardView.context

            val id = getDrawableInt(context, place.imageName)
            setImageToImageView(context, viewHolder.upcomingListItemIcon, id)

            viewHolder.upcomingAddress1.text = place.type
            viewHolder.upcomingAddress2.text = place.address
            viewHolder.upcomingMobile.text = place.phone
            viewHolder.upcomingDetails.text = place.desc

            /*viewHolder.placeRatingBar.rating = java.lang.Float.parseFloat(place.totalRating)
            viewHolder.totalRatingTextView.text = place.totalRating
            viewHolder.ratingCountTextView.text = place.ratingCount*/

            /*if (Integer.parseInt(place.discount) > 0) {
                viewHolder.promoCardView.visibility = View.VISIBLE
                val discount = place.discount + " %"
                viewHolder.promoAmtTextView.text = discount
            } else {
                viewHolder.promoCardView.visibility = View.GONE
            }*/


            viewHolder.placeHolderCardView.setOnClickListener { v: View -> itemClickListener.onItemClick(v, placeArrayList[position], position) }


        }
    }

    override fun getItemCount(): Int {
        return placeArrayList.size
    }

    inner class PlaceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var upcomingListItemIcon: ImageView = view.upcomingListItemIcon
        var upcomingUserName: TextView = view.upcomingUserName
        var upcomingAddress1: TextView = view.upcomingAddress1
        var upcomingAddress2: TextView = view.upcomingAddress2
        var upcomingMobile: TextView = view.upcomingMobile
        var upcomingDetails: TextView = view.upcomingDetail
        var placeHolderCardView: CardView = view.placeHolderCardView

        /*var totalRatingTextView: TextView = view.totalRatingTextView
        var ratingCountTextView: TextView = view.ratingCountTextView
        var placeRatingBar: RatingBar = view.placeRatingBar
        var promoAmtTextView: TextView = view.promoAmtTextView
        var promoCardView: CardView = view.promoCardView
        var placeHolderCardView: CardView = view.placeHolderCardView*/

    }

    fun setImageToImageView(context: Context, imageView: ImageView, drawable: Int) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
            .skipMemoryCache(true)

        /*Glide.with(context)
            .load(drawable)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageView)*/
    }

    fun getDrawableInt(context: Context?, name: String?): Int {
        return context?.resources!!.getIdentifier(name, "drawable", context.packageName)
    }
}
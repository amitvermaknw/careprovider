package com.example.careproviderapp

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.careproviderapp.adapter.UpcomingApptAdapter
import com.example.careproviderapp.helpers.Place
import com.example.careproviderapp.utility.PlaceRepository
import java.util.ArrayList

class UpcomingAppt : Fragment() {
    internal lateinit var placeArrayList: ArrayList<Place>
    internal lateinit var adapter: UpcomingApptAdapter
    internal lateinit var view: View
    internal lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view = inflater!!.inflate(R.layout.activity_upcoming_appt, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.placeList2RecyclerView)

        initData()
        initUI()
        initDataBindings()
        initActions()

        return view
    }

    companion object {
        fun newInstance(): UpcomingAppt = UpcomingAppt()
    }

    private fun initData() {
        placeArrayList = PlaceRepository.placeList
    }

    private fun initUI() {
        // get list adapter
        adapter = UpcomingApptAdapter(placeArrayList)

        // get recycler view
        val mLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
    }

    private fun initDataBindings() {
        // bind adapter to recycler
        recyclerView.adapter = adapter
    }

    private fun initActions() {
        adapter.setOnItemClickListener(object : UpcomingApptAdapter.OnItemClickListener {
            override fun onItemClick(view: View, obj: Place, position: Int) {
                //Toast.makeText(activity, "Selected : " + obj.name, Toast.LENGTH_LONG).show()
                val intent = Intent(activity, AppointmentsDetail::class.java);
                startActivity(intent);
            }

        })

    }
}


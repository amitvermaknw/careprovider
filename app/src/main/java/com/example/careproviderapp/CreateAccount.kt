package com.example.careproviderapp

import android.app.ActionBar
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.careproviderapp.helpers.InputValidation
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class CreateAccount : AppCompatActivity() {

    private val activity = this@CreateAccount;
    private lateinit var inputValidation: InputValidation;

    private lateinit var txtInputLayoutUserName: TextInputLayout;
    private lateinit var txtInputEditUserName: TextInputEditText;
    private lateinit var txtInputLayoutFirstName: TextInputLayout;
    private lateinit var txtInputEditFirstName: TextInputEditText;
    private lateinit var txtInputLayoutLastName: TextInputLayout;
    private lateinit var txtInputEditLastName: TextInputEditText;
    private lateinit var txtInputLayoutAddress: TextInputLayout;
    private lateinit var txtInputEditAddress: TextInputEditText;
    private lateinit var txtInputLayoutCity: TextInputLayout;
    private lateinit var txtInputEditCity: AutoCompleteTextView;
    private lateinit var txtInputLayoutState: TextInputLayout
    private lateinit var txtInputEditState: AutoCompleteTextView;
    private lateinit var txtInputLayoutMobile: TextInputLayout;
    private lateinit var txtInputEditMobile: TextInputEditText;
    private lateinit var btnCreateAccount: Button;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account);

        supportActionBar!!.title = "Create Account";
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        initObjects();
        initView();
    }

    private fun initObjects() {

        inputValidation = InputValidation(activity);

    }

    private fun initView() {
        txtInputLayoutUserName = findViewById<View>(R.id.txtInputLayoutUserName) as TextInputLayout;
        txtInputLayoutFirstName = findViewById<View>(R.id.txtInputLayoutFirstName) as TextInputLayout;
        txtInputLayoutLastName = findViewById<View>(R.id.txtInputLayoutLastName) as TextInputLayout;
        txtInputLayoutAddress = findViewById<View>(R.id.txtInputLayoutAddress) as TextInputLayout;
        txtInputLayoutCity = findViewById<View>(R.id.txtInputLayoutCity) as TextInputLayout;
        txtInputLayoutState = findViewById<View>(R.id.txtInputLayoutState) as TextInputLayout;
        txtInputLayoutMobile = findViewById<View>(R.id.txtInputLayoutMobile) as TextInputLayout;

        txtInputEditUserName = findViewById<View>(R.id.txtInputEditUserName) as TextInputEditText;
        txtInputEditFirstName = findViewById<View>(R.id.txtInputEditFirstName) as TextInputEditText;
        txtInputEditLastName = findViewById<View>(R.id.txtInputEditLastName) as TextInputEditText;
        txtInputEditAddress = findViewById<View>(R.id.txtInputEditAddress) as TextInputEditText;
        txtInputEditCity = findViewById<View>(R.id.txtInputEditCity) as AutoCompleteTextView;
        txtInputEditState = findViewById<View>(R.id.txtInputEditState) as AutoCompleteTextView;
        txtInputEditMobile = findViewById<View>(R.id.txtInputEditMobile) as TextInputEditText;

        btnCreateAccount = findViewById<View>(R.id.btnCreateAccount) as Button;

        textInputListner();

        btnCreateAccount.setOnClickListener {

            val intent = Intent(this@CreateAccount, UploadAadhaar::class.java);
            startActivity(intent);
            
            /*inputValidation.isInputEditTextFilled(txtInputEditUserName!!, txtInputLayoutUserName!!, "User name is required.");
            inputValidation.isInputEditTextFilled(txtInputEditFirstName!!, txtInputLayoutFirstName!!, "First name is required.");
            inputValidation.isInputEditTextFilled(txtInputEditLastName!!, txtInputLayoutLastName!!, "Last name is required.");
            //inputValidation.isInputEditTextFilled(txtInputEditAddress!!, txtInputLayoutAddress!!, "Address is required.");
            //inputValidation.isInputEditTextFilled(txtInputEditCity!!, txtInputLayoutUserName!!, "User name is required.");
            //inputValidation.isInputEditTextFilled(txtInputEditState!!, txtInputLayoutUserName!!, "User name is required.");
            inputValidation.isInputEditTextFilled(txtInputEditMobile!!, txtInputLayoutMobile!!, "Mobile number is required.");*/
        }
    }
    
    private fun textInputListner() {
        txtInputEditUserName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        }

        txtInputEditUserName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditUserName!!, txtInputLayoutUserName!!, "User name is required.");
            }
        });

        txtInputEditFirstName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        }

        txtInputEditFirstName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditFirstName!!, txtInputLayoutFirstName!!, "First name is required.");
            }
        });

        txtInputEditLastName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        }

        txtInputEditLastName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditLastName!!, txtInputLayoutLastName!!, "Last name is required.");
            }
        });

        txtInputEditAddress.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        }

        /*txtInputEditAddress.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditAddress!!, txtInputLayoutAddress!!, "Address is required.");
            }
        });*/

        txtInputEditMobile.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        }

        txtInputEditMobile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditMobile!!, txtInputLayoutMobile!!, "Mobile number is required.");
            }
        });

        txtInputEditAddress.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                //val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, languages)
                //txtInputEditAddress.setAdapter(adapter)

                //inputValidation.isInputEditTextFilled(txtInputEditMobile!!, txtInputLayoutMobile!!, "Mobile number is required.");
            }
        });

    }
}

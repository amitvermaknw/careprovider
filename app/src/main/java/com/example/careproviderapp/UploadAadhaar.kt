package com.example.careproviderapp

import android.R.attr
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator


class UploadAadhaar : AppCompatActivity() {

    private lateinit var btnScanAadhaar: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload__aadhaar)

        supportActionBar!!.title = "Upload Aadhaar card";

        btnScanAadhaar = findViewById<View>(R.id.btnScanAadhaar) as Button;

        btnScanAadhaar.setOnClickListener {

            val scanner = IntentIntegrator(this);
            scanner.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK) {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents == null) {
                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
}

package com.example.careproviderapp

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.careproviderapp.helpers.InputValidation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val activity = this@MainActivity;
    private lateinit  var inputValidation: InputValidation;

    private lateinit var txtInputLayoutUserName: TextInputLayout;
    private lateinit var txtInputLayoutPassword: TextInputLayout;
    private lateinit var txtInputEditTextUserName: TextInputEditText;
    private lateinit var txtInputEditTextPassword: TextInputEditText;
    private lateinit var btnSignup: Button;

    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide();

        initObjects();
        initView();

    }

    private fun initObjects() {

        inputValidation = InputValidation(activity);

    }

    private fun initView() {

        //Get Reference to all component
        txtInputLayoutUserName = findViewById<View>(R.id.txtInputLayoutUserName) as TextInputLayout;
        txtInputLayoutPassword = findViewById<View>(R.id.txtInpputLayoutPassword) as TextInputLayout;

        txtInputEditTextUserName = findViewById<View>(R.id.txtInputEditUserName) as TextInputEditText;
        txtInputEditTextPassword = findViewById<View>(R.id.txtInputEditPassword) as TextInputEditText;


        btnSignup = findViewById<View>(R.id.btnLogin) as Button;


        logo.setImageResource(R.drawable.medical)

        txtInputEditTextUserName.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        });

        txtInputEditTextPassword.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inputValidation.hideKeyboard(v);
            }
        });

        txtInputEditTextUserName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditTextUserName!!, txtInputLayoutUserName!!, "User name is required.");
            }
        });

        txtInputEditTextPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputValidation.isInputEditTextFilled(txtInputEditTextPassword!!, txtInputLayoutPassword!!, "Password is required.");
            }
        })


        btnSignup.setOnClickListener {

            appointment()

            //inputValidation.isInputEditTextFilled(txtInputEditTextUserName!!, txtInputLayoutUserName!!, "User name is required.");
            //inputValidation.isInputEditTextFilled(txtInputEditTextPassword!!, txtInputLayoutPassword!!, "Password is required.");

        }

        setupGoogle()
    }

    private fun setupGoogle() {
        //setContentView(R.layout.activity_main)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        btnCreateUsingGmail.setOnClickListener {
            val signInIntent: Intent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task);
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            Log.d("Signin successfully", "signin failed")
            // Signed in successfully, show authenticated UI.
            //updateUI(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            //Log.w(FragmentActivity.TAG, "signInResult:failed code=" + e.statusCode)
            //updateUI(null)

            Log.d("Signin failed", "signin failed")
        }
    }

    public fun createAccount(view: View) {
        val intent = Intent(this@MainActivity, CreateAccount::class.java);
        startActivity(intent);
    }

    private fun appointment() {
        val intent = Intent(this@MainActivity, Dashboard::class.java);
        startActivity(intent);
    }

}

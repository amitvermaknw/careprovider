package com.example.careproviderapp.utility

import com.example.careproviderapp.helpers.DashboardCategory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.ArrayList

object DashboardUtility {

    val categoryList: ArrayList<DashboardCategory>
        get() = Gson().fromJson<ArrayList<DashboardCategory>>(json, object : TypeToken<ArrayList<DashboardCategory>>() {

        }.type)

    private val json = "[\n" +
            "  {\n" +
            "    \"name\" : \"Appointment\",\n" +
            "    \"image\" : \"appoint\",\n" +
            "    \"color\" : \"#4683d4\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\" : \"Profile\",\n" +
            "    \"image\" : \"profile\",\n" +
            "    \"color\" : \"#2b6559\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\" : \"Wallet\",\n" +
            "    \"image\" : \"payment\",\n" +
            "    \"color\" : \"#4196a8\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\" : \"Chat\",\n" +
            "    \"image\" : \"chat\",\n" +
            "    \"color\" : \"#5618ab\"\n" +
            "  }\n" +
            "]"

}

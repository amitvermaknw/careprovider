package com.example.careproviderapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_appointments_detail.*

class AppointmentsDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointments_detail)

        supportActionBar!!.title = "Appointment detail";

        profileImageView.setImageResource(R.drawable.ic_action_user)
    }

}

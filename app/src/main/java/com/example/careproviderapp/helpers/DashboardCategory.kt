package com.example.careproviderapp.helpers

import com.google.gson.annotations.SerializedName

class DashboardCategory(@field:SerializedName("name")
                             var name: String, @field:SerializedName("image")
                             var image: String, @field:SerializedName("color")
                             var color: String)
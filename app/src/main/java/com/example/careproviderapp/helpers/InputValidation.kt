package com.example.careproviderapp.helpers

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class InputValidation

    (private val context: Context) {

        fun isInputEditTextFilled(textInputEditText: TextInputEditText, textInputLayout: TextInputLayout, message: String): Boolean {
            val value = textInputEditText.text.toString().trim()
            if (value.isEmpty()) {
                textInputLayout.error = message
                hideKeyboard(textInputEditText)
                return false
            } else {
                textInputLayout.isErrorEnabled = false
            }

            return true
        }

    fun hideKeyboard(view: View) {
        val inputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager!!.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
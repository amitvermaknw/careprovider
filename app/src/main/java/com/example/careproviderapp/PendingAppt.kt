package com.example.careproviderapp
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.careproviderapp.adapter.PendingApptAdapter
import com.example.careproviderapp.helpers.Place
import com.example.careproviderapp.utility.PlaceRepository
import java.util.ArrayList

class PendingAppt : Fragment() {

    internal lateinit var placeArrayList: ArrayList<Place>
    internal lateinit var adapter: PendingApptAdapter
    internal lateinit var view: View
    internal lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view = inflater!!.inflate(R.layout.activity_pending_appt, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.placeList1RecyclerView)

        initData()
        initUI()
        initDataBindings()
        initActions()

        return view
    }

    companion object {
        fun newInstance(): PendingAppt = PendingAppt()
    }

    private fun initData() {
        placeArrayList = PlaceRepository.placeList
    }

    private fun initUI() {
        //initToolbar()

        // get list adapter
        adapter = PendingApptAdapter(placeArrayList)

        // get recycler view
        val mLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
    }

    private fun initDataBindings() {
        // bind adapter to recycler
        recyclerView.adapter = adapter
    }

    private fun initActions() {
        adapter.setOnItemClickListener(object : PendingApptAdapter.OnItemClickListener {
            override fun onItemClick(view: View, obj: Place, position: Int) {
                //Toast.makeText(activity, "Selected : " + obj.name, Toast.LENGTH_LONG).show()
                val intent = Intent(activity, AppointmentsDetail::class.java);
                startActivity(intent);
            }

        })

    }

    //region Init Toolbar
    /*private fun initToolbar() {

        toolbar.setNavigationIcon(R.drawable.baseline_menu_black_24)

        if (toolbar.navigationIcon != null) {
            toolbar.navigationIcon?.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP)
        }

        toolbar.title = "Place List 1"

        try {
            toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.md_white_1000))
        } catch (e: Exception) {
            Log.e("TEAMPS", "Can't set color.")
        }

        try {
            setSupportActionBar(toolbar)
        } catch (e: Exception) {
            Log.e("TEAMPS", "Error in set support action bar.")
        }

        try {
            if (supportActionBar != null) {
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
        } catch (e: Exception) {
            Log.e("TEAMPS", "Error in set display home as up enabled.")
        }

    }*/

}


package com.example.careproviderapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_appointments.*

class Appointments : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointments)

        supportActionBar!!.title = "Appointment";

        val tabLayout: TabLayout = findViewById(R.id.AppointmentTabs)
        val viewPager: ViewPager = findViewById(R.id.ApptViewPager)
        tabLayout.setupWithViewPager(viewPager)

        val adapter = AppointmentAdapter(supportFragmentManager)
        viewPager.adapter = adapter

        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)!!.setIcon(android.R.drawable.ic_dialog_info)
        tabLayout.getTabAt(1)!!.setIcon(android.R.drawable.ic_popup_reminder)

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        bottomNavigationPanel.setOnNavigationItemSelectedListener{item: MenuItem ->
            when (item.itemId) {
                R.id.appointmentMenu -> {
                    val intent = Intent(this@Appointments, Appointments::class.java);
                    startActivity(intent);
                }

                R.id.dashboardMenu -> Toast.makeText(applicationContext, "Dashboard Menu is selected.",
                    Toast.LENGTH_LONG).show()

                R.id.walletMenu -> {
                    val intent = Intent(this@Appointments, Wallet::class.java);
                    startActivity(intent);
                }

                R.id.profileMenu -> {
                    val intent = Intent(this@Appointments, Profile::class.java);
                    startActivity(intent);
                }

                R.id.logoutMenu -> {
                    val intent = Intent(this@Appointments, MainActivity::class.java);
                    startActivity(intent);
                }

            }
            false

        }
    }
}

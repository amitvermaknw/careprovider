package com.example.careproviderapp

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AppointmentAdapter (fm: FragmentManager) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private lateinit var data: Fragment

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> PendingAppt.newInstance()
        1 -> UpcomingAppt.newInstance()
        else -> data
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> "Pending"
        1 -> "Upcoming"
        else -> ""
    }

    override fun getCount(): Int = 2

}

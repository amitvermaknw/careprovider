package com.example.careproviderapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.example.careproviderapp.adapter.DashboardAdapter
import com.example.careproviderapp.helpers.DashboardCategory
import com.example.careproviderapp.utility.DashboardUtility
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.util.ArrayList

class Dashboard : AppCompatActivity() {

    private var context: Context? = null
    private lateinit var catArrayList: ArrayList<DashboardCategory>
    private lateinit var mAdapter: DashboardAdapter
    internal var numberOfColumns = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        supportActionBar!!.title = "Dashboard";

        initData()

        initUI()

        initDataBinding()

        initActions()

    }

    private fun initData() {

        catArrayList = DashboardUtility.categoryList

    }

    private fun initUI() {

        //initToolbar()

        mAdapter = DashboardAdapter(catArrayList)

        // get RecyclerView and bind
        recyclerView.layoutManager = GridLayoutManager(this, numberOfColumns)
        recyclerView.itemAnimator = DefaultItemAnimator()


    }

    private fun initDataBinding() {
        // get data and adapter
        recyclerView.adapter = mAdapter
    }

    private fun initActions() {

        mAdapter.setOnItemClickListener(object : DashboardAdapter.OnItemClickListener {
            override fun onItemClick(view: View, obj: DashboardCategory, position: Int) {
                //Toast.makeText(this@Dashboard, "Selected : " + obj.name, Toast.LENGTH_LONG).show()
                val intent = Intent(this@Dashboard, Appointments::class.java);
                startActivity(intent);
            }
        })
    }

}

package com.example.careproviderapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_appointments.*

class Wallet : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)

        supportActionBar!!.title = "Wallet";

        initBottomMenu()
    }

    fun initBottomMenu() {
        bottomNavigationPanel.setOnNavigationItemSelectedListener{item: MenuItem ->
            when (item.itemId) {
                R.id.appointmentMenu -> {
                    val intent = Intent(this@Wallet, Appointments::class.java);
                    startActivity(intent);
                }

                R.id.dashboardMenu -> Toast.makeText(applicationContext, "Dashboard Menu is selected.",
                    Toast.LENGTH_LONG).show()

                R.id.walletMenu -> {
                    val intent = Intent(this@Wallet, Wallet::class.java);
                    startActivity(intent);
                }

                R.id.profileMenu -> {
                    val intent = Intent(this@Wallet, Profile::class.java);
                    startActivity(intent);
                }

                R.id.logoutMenu -> {
                    val intent = Intent(this@Wallet, MainActivity::class.java);
                    startActivity(intent);
                }

            }
            false

        }
    }
}
